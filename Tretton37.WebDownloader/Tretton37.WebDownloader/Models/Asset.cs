﻿namespace Tretton37.WebDownloader.Models
{
    public class Asset
    {
        public AssteType AssetType { get; set; }
        public string Name { get; set; }
        public string MetaData { get; set; }
        public string URL { get; set; }
    }
}
