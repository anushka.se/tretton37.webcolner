﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Tretton37.WebDownloader.Helpers;
using Tretton37.WebDownloader.Models;

namespace Tretton37.WebDownloader
{

    class Program
    {

        const string BASE_URL = "https://tretton37.com";
        static string TARGET_DOWNLOAD_BASE_DIR = @"C:\t\"; // For now change which folder to save
        static void Main(string[] args)
        {
            DownloadWebPage(BASE_URL).GetAwaiter().GetResult();
            Console.ReadKey();
        }

        /// <summary>
        /// Get list of files to download using href and src
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static List<Asset> AnalyseAssetsAndLinks(string input)
        {
            //TODO: Add all possible extensions
            var allowedFileExtensions = new List<string>() { ".css", ".png", ".jpg", ".jpeg", ".txt", ".js", ".woff" };
            Regex regex = new Regex("href\\s*=\\s*(?:\"(?<1>[^\"]*)\"|(?<1>\\S+))|src\\s*=\\s*(?:\"(?<1>[^\"]*)\"|(?<1>\\S+))", RegexOptions.IgnoreCase);
            Match match;
            List<Asset> assetsList = new List<Asset>();
            for (match = regex.Match(input); match.Success; match = match.NextMatch())
            {
                var asset = match.Groups.Cast<Group>().ToList().Last(o => o.Value != "").ToString();

                if (asset.Contains("http") || asset.Contains("https") || asset == "/" || asset.Contains("#") || string.IsNullOrEmpty(asset))
                {
                    continue;
                }

                if (allowedFileExtensions.Any(f => asset.Contains(f)))
                {
                    assetsList.Add(new Asset { AssetType = AssteType.File, MetaData = asset, URL = BASE_URL.TrimEnd('/') + "/" + asset.TrimStart('/') });
                }
                else
                {
                    assetsList.Add(new Asset { AssetType = AssteType.WebPages, MetaData = asset, URL = BASE_URL.TrimEnd('/') + "/" + asset.TrimStart('/') });
                }
            }

            return assetsList;
        }

        /// <summary>
        /// main executer for web page download
        /// </summary>
        /// <param name="webURL"></param>
        /// <returns></returns>
        private static async Task DownloadWebPage(string webURL)
        {
            try
            {
                string pageData = WebPageDownloadHelper.StreamWebPagesAsString(webURL);

                var assetsList = AnalyseAssetsAndLinks(pageData);

                var _pageName = webURL.Split('/').LastOrDefault();
                _pageName = !string.IsNullOrEmpty(_pageName) && webURL.Split('/').Count() > 3 ? _pageName : "index";

                //TODO: Consider about folder path of pages
                var saveStaticWebPagesTask =  WebPageDownloadHelper.SaveStaticWebPagesAsync(pageData, TARGET_DOWNLOAD_BASE_DIR + _pageName + ".html");
                var downloadAssetFilesTask = DownloadAssetFilesTaskAsync(assetsList.Where(i => i.AssetType == AssteType.File).ToList());

                var webPagesList = assetsList.Where(i => i.AssetType == AssteType.WebPages).ToList();
                saveStaticWebPagesTask.GetAwaiter();

                foreach (var i in webPagesList)
                {
                    await DownloadWebPage(i.URL);
                }

                downloadAssetFilesTask.GetAwaiter();
            }
            catch (Exception ex)
            {
                Console.WriteLine("DownloadWebPage Ran into an error: {0}", ex.Message);
            }
        }

        #region DownloadAssets

        /// <summary>
        /// to download an asset file
        /// </summary>
        /// <param name="url"></param>
        /// <param name="saveToDirectory"></param>
        /// <returns></returns>
        private static async Task DownloadFileAsync(string url, string saveToDirectory)
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;

                    webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(FileDownloadEventCompleted);

                    webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressChangedEvent);

                    //remove bulk data from file name
                    saveToDirectory = saveToDirectory.Split('?')[0];

                    var fullPath = string.IsNullOrEmpty(TARGET_DOWNLOAD_BASE_DIR) ?
                        Environment.CurrentDirectory : TARGET_DOWNLOAD_BASE_DIR + saveToDirectory;
                    var pathWithoutFileName = Directory.GetParent(fullPath);

                    if (!Directory.Exists(pathWithoutFileName.ToString()))
                    {
                        Directory.CreateDirectory(pathWithoutFileName.ToString());
                    }

                    await webClient.DownloadFileTaskAsync(new Uri(url), fullPath);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("DownloadFile: {0} Ran into an error: {0}", url, ex.Message);
            }
        }
        /// <summary>
        /// To catch file download event when completed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void FileDownloadEventCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Console.WriteLine("File has been downloaded.");
        }
        /// <summary>
        /// delegate for catch Download Progress event change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void DownloadProgressChangedEvent(object sender, DownloadProgressChangedEventArgs e)
        {
            Console.WriteLine($"Download status: {e.ProgressPercentage}%.");
        }
        
        /// <summary>
        /// to download set of files asyncly
        /// </summary>
        /// <param name="assets"></param>
        /// <returns></returns>
        private static async Task DownloadAssetFilesTaskAsync(List<Asset> assets)
        {
            await Task.WhenAll(assets.Select(a => DownloadFileAsync(a.URL, a.MetaData)));
        }
        #endregion

    }
}
