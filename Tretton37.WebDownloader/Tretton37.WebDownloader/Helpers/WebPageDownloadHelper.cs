﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Tretton37.WebDownloader.Helpers
{
    public static class WebPageDownloadHelper
    {
        public static string StreamWebPagesAsString(string theURL)
        {
            try
            {
                WebClient client = new WebClient();
                //client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                Stream data = client.OpenRead(theURL);
                StreamReader reader = new StreamReader(data);
                string pageAsHtmlString = reader.ReadToEnd();
                return pageAsHtmlString;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public static async Task SaveStaticWebPagesAsync(string data, string destination)
        {
            using (var sw = new StreamWriter(destination))
            {
                await sw.WriteAsync(data);
            }
        }
    }
}
